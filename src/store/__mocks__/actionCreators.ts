import { ActionTypes } from "../actionTypes";
export const handleSearch = () => {
  return (dispatch: DispatchType) => {
    const characters: Result<Character> = {
      results: [
        { id: 1, thumbnail: { extension: "ext", path: "path" } },
        { id: 2, thumbnail: { extension: "ext", path: "path" } },
        { id: 3, thumbnail: { extension: "ext", path: "path" } },
      ],
      total: 3,
    };

    dispatch({ type: ActionTypes.SET_CHARACTERS, characters });
  };
};

export const handleSearchSeries = (selectedCharacter: Character) => {
  return (dispatch: DispatchType) => {
    const series: Result<Serie> = {
      results: [
        {
          id: "1",
          name: "name1",
          thumbnail: { extension: "ext", path: "path" },
        },
        {
          id: "2",
          name: "name2",
          thumbnail: { extension: "ext", path: "path" },
        },
        {
          id: "3",
          name: "name3",
          thumbnail: { extension: "ext", path: "path" },
        },
      ],
      total: 3,
    };

    dispatch({
      type: ActionTypes.SET_SERIES,
      characters: { results: [], total: 0 },
      selectedCharacter,
      series: series,
    });
  };
};

export const handleSelectedCharacter = (character: Character) => {
  return (dispatch: DispatchType) => {
    dispatch({
      type: ActionTypes.SET_SELECTED_CHARACTER,
      selectedCharacter: character,
    });
  };
};

export const handleEditcharacter = (character: Character) => {
  return (dispatch: DispatchType) => {
    dispatch({
      type: ActionTypes.EDIT_SELECTED_CHARACTER,
      selectedCharacter: character,
    });
  };
};
