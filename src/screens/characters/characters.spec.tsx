import { Store } from "redux";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";

import { render, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import { BrowserRouter as Router } from "react-router-dom";

import Characters from "./characters";

jest.mock("../../store/actionCreators");

const state = {
  data: {
    results: [
      {
        id: 1234,
        thumbnail: {
          extension: "ext",
          path: "path",
        },
      },
    ],
    total: 0,
  },
  selectedCharacter: undefined,
  series: {
    results: [],
    total: 0,
  },
};

const buildStore = configureStore([thunk]);

describe("characters screen", () => {
  let store: Store;
  beforeEach(() => {
    store = buildStore(state);
  });

  test("rendering screen with a card", () => {
    const screen = render(
      <Provider store={store}>
        <Characters />
      </Provider>
    );

    const img = screen.container.querySelector(
      '[class="card-character-image"]'
    ) as HTMLImageElement;
    expect(screen.container).toBeDefined();
    expect(img.src).toStrictEqual("http://localhost/path.ext");
    expect(screen).toMatchSnapshot();
  });
});
