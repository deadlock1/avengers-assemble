import Biography from "./biography";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store } from "redux";
import thunk from "redux-thunk";
import configureStore from "redux-mock-store";
import { initialState } from "../../store/reducer";
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";

jest.mock("../../api/driver");

const buildStore = configureStore([thunk]);

describe("biography screen", () => {
  let store: Store;
  beforeEach(() => {
    store = buildStore(initialState);
  });

  test("rendering screen", () => {
    const screen = render(
      <Provider store={store}>
        <Biography />
      </Provider>
    );

    expect(screen).toMatchSnapshot();
  });

  test("button text must change on click", () => {
    const screen = render(
      <Provider store={store}>
        <Biography />
      </Provider>
    );

    const buttonEdit = screen.getByText("EDIT") as HTMLButtonElement;
    userEvent.click(buttonEdit);
    act(() => {
      const inputName = screen.getByTitle("character-name-edit");
      userEvent.type(inputName, "iron man");
      expect(inputName).toHaveValue("iron man");
    });

    act(() => {
      const inputDescription = screen.getByTitle("character-name-description");
      userEvent.type(inputDescription, "iron man");
      expect(inputDescription).toHaveValue("iron man");
    });
  });
});
