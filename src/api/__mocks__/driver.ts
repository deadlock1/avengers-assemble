import { AxiosInstance } from "axios";
import axiosInstance from "../config";

class Driver {
  public constructor(
    private readonly httpClient: AxiosInstance = axiosInstance
  ) {}

  public getCharacters(
    pagination?: Pagination,
    query?: Query
  ): Promise<CharactersResponse> {
    return Promise.resolve({
      data: {
        results: [
          {
            id: 123,
            thumbnail: {
              extension: "123",
              path: "path",
            },
          },
        ],
        total: 1,
      },
    });
  }

  public getBiography(id: string): Promise<CharacterResponse> {
    return Promise.resolve({
      data: {
        results: [
          {
            description: "description",
            name: "name",
            thumbnail: {
              extension: "ext",

              path: "path",
            },
          },
        ],
      },
    });
  }

  public getCharacterSeries = async (
    pagination: Pagination,
    id: string
  ): Promise<SeriesResponse> => {
    return Promise.resolve({
      data: {
        results: [
          {
            id: "id",
            name: "name",
            thumbnail: {
              extension: "ext",
              path: "path",
            },
          },
        ],
        total: 1,
      },
    });
  };
}

export default new Driver();
