import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import { App } from "./App";
import { applyMiddleware, createStore, Store } from "redux";
import thunk from "redux-thunk";
import reducer from "./store/reducer";

const store: Store<RootState, CharacterAction> & {
  dispatch: DispatchType;
} = createStore(reducer, applyMiddleware(thunk));

describe("Test context", () => {
  test("test", () => {
    render(
      <Provider store={store}>
        <App />
      </Provider>
    );

    expect(screen).toBeTruthy();
  });
});
