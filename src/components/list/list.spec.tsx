import List from "./list";
import { render, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { Provider } from "react-redux";
import { Store } from "redux";
import thunk from "redux-thunk";

import configureMockStore from "redux-mock-store";
import { initialState } from "../../store/reducer";
import { BrowserRouter as Router } from "react-router-dom";
import { ActionTypes } from "../../store/actionTypes";

const middlewares = [thunk];

const mockStore = configureMockStore(middlewares);

let store: Store;
beforeEach(() => {
  store = mockStore(initialState);
});

describe("list specs", () => {
  test("rendering list with src for img", () => {
    const { container } = render(
      <Provider store={store}>
        <List
          handleSelect={jest.fn()}
          data={[
            { id: 123, thumbnail: { extension: "jpg", path: "localhost:300" } },
          ]}
        />
      </Provider>
    );

    const imgElement = container.querySelector(
      '[class="card-character-image"]'
    ) as HTMLImageElement;

    expect(imgElement.src).toStrictEqual("localhost:300.jpg");
  });

  test("should call handleSelect function when list item is clicked", async () => {
    const handle = jest.fn().mockImplementation((character: Character) => {
      return (dispatch: DispatchType) => {
        dispatch({
          type: ActionTypes.SET_SELECTED_CHARACTER,
          selectedCharacter: character,
        });
      };
    });

    const { container } = render(
      <Router>
        <Provider store={store}>
          <List
            handleSelect={handle}
            data={[
              {
                id: 123,
                thumbnail: { extension: "jpg", path: "localhost:300" },
              },
            ]}
          />
        </Provider>
      </Router>
    );

    const characterCard = container.querySelector(
      '[class="card-wrapper"]'
    ) as HTMLDivElement;

    await waitFor(() => {
      userEvent.click(characterCard);
    });

    expect(handle).toHaveBeenCalled();
  });
});
