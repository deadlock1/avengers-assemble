import Card from "./card";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { Store } from "redux";
import thunk from "redux-thunk";
import configureStore from "redux-mock-store";
import { initialState } from "../../store/reducer";
import userEvent from "@testing-library/user-event";

jest.mock("../../api/driver");

const buildStore = configureStore([thunk]);

describe("header component", () => {
  let store: Store;
  beforeEach(() => {
    store = buildStore(initialState);
  });

  test("two way binding input value", () => {
    const screen = render(
      <Provider store={store}>
        <Card extension="ext" id={1} path="path" />
      </Provider>
    );

    expect(screen).toMatchSnapshot();
  });
});
